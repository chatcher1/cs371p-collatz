// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----
// read
// ----

TEST(CollatzFixture, read) {
    ASSERT_EQ(collatz_read("1 10\n"), make_pair(1, 10));
}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    ASSERT_EQ(collatz_eval(make_pair(1, 10)), make_tuple(1, 10, 20));
}

TEST(CollatzFixture, eval1) {
    ASSERT_EQ(collatz_eval(make_pair(100, 200)), make_tuple(100, 200, 125));
}

TEST(CollatzFixture, eval2) {
    ASSERT_EQ(collatz_eval(make_pair(201, 210)), make_tuple(201, 210, 89));
}

TEST(CollatzFixture, eval3) {
    ASSERT_EQ(collatz_eval(make_pair(900, 1000)), make_tuple(900, 1000, 174));
}

// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 10, 20));
    ASSERT_EQ(sout.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream sin("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n");
}



//Caroline's unit tests
TEST(CollatzFixture, unit1) {
 ASSERT_EQ(collatz_read("10 10\n"), make_pair(10, 10));}

 TEST(CollatzFixture, unit2){
    ostringstream sout;
    collatz_print(sout, make_tuple(1000, 1300, 40));
    ASSERT_EQ(sout.str(), "1000 1300 40\n");
}
TEST(CollatzFixture, unit3) {
    ASSERT_EQ(collatz_eval(make_pair(850680, 499652)), make_tuple(850680,499652,525));
}
TEST(CollatzFixture, unit4){
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 1, 1));
    ASSERT_EQ(sout.str(), "1 1 1\n");
}

TEST(CollatzFixture, unit5){
    ostringstream sout;
    collatz_print(sout, make_tuple(850680, 499652, 525));
    ASSERT_EQ(sout.str(), "850680 499652 525\n");
}
 
TEST(CollatzFixture, unit6) {
 ASSERT_EQ(collatz_read("111110 111110\n"), make_pair(111110, 111110));}

 TEST(CollatzFixture, unit7) {
    istringstream sin("1 1\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 1 1\n");
 }
 TEST(CollatzFixture, unit8) {
    istringstream sin("6 10\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "6 10 20\n");
 }
TEST(CollatzFixture, unit9) {
    istringstream sin("50 200\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "50 200 125\n");
 }

 TEST(CollatzFixture, unit10) {
    istringstream sin("200 50\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "200 50 125\n");
 }
 TEST(CollatzFixture, unit11) {
    istringstream sin("10 1\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "10 1 20\n");
 }
TEST(CollatzFixture, unit12) {

    ASSERT_EQ(collatz_eval(make_pair(8, 8)), make_tuple(8, 8, 4));
 }

TEST(CollatzFixture, unit13) {
    istringstream sin("8618 322139\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "8618 322139 443\n");
 }

 TEST(CollatzFixture, unit14) {

    ASSERT_EQ(collatz_eval(make_pair(4, 2)), make_tuple(4, 2, 8));
 }
 
 



// TEST(CollatzFixture, read) {
//     ASSERT_EQ(collatz_read("10 10000\n"), make_pair(10, 10000));}
//Caroline's acceptance tests

// TEST(CollatzFixture, solve) {
//     istringstream sin("1 1\n8 8\n5 5\n");
//     ostringstream sout;
//     collatz_solve(sin, sout);
//     ASSERT_EQ(sout.str(), "1 1 1\n8 8 4\n5 5 6\n");}

// TEST(CollatzFixture, solve) {
//     istringstream sin("1 1000\n500 1000\n");
//     ostringstream sout;
//     collatz_solve(sin, sout);
//     ASSERT_EQ(sout.str(), "1 1 1\n8 8 4\n5 5 6\n");}

//     TEST(CollatzFixture, solve) {
//     istringstream sin("1 1000\n500 1000\n");
//     ostringstream sout;
//     collatz_solve(sin, sout);
//     ASSERT_EQ(sout.str(), "1 1 1\n8 8 4\n5 5 6\n");}

//     TEST(CollatzFixture, solve) {
//     istringstream sin("1 999999\n1 999999\n1 999999\n1 999999\n");
//     ostringstream sout;
//     collatz_solve(sin, sout);
//     ASSERT_EQ(sout.str(), "1 1 1\n8 8 4\n5 5 6\n");}

//     TEST(CollatzFixture, solve) {
//     istringstream sin("1 10000\n50 10000\n");
//     ostringstream sout;
//     collatz_solve(sin, sout);
//     ASSERT_EQ(sout.str(), "1 1 1\n8 8 4\n5 5 6\n");}

//     TEST(CollatzFixture, solve) {
//     istringstream sin("4513 7919\n");
//     ostringstream sout;
//     collatz_solve(sin, sout);
//     ASSERT_EQ(sout.str(), "1 1 1\n8 8 4\n5 5 6\n");}

// TEST(CollatzFixture, solve) {
//     istringstream sin("7541 7457\n7577 7919\n");
//     ostringstream sout;
//     collatz_solve(sin, sout);
//     ASSERT_EQ(sout.str(), "1 1 1\n8 8 4\n5 5 6\n");}

// TEST(CollatzFixture, solve) {
//     istringstream sin("451300 791900\n");
//     ostringstream sout;
//     collatz_solve(sin, sout);
//     ASSERT_EQ(sout.str(), "1 1 1\n8 8 4\n5 5 6\n");}

// TEST(CollatzFixture, solve) {
//     istringstream sin("453 454\n");
//     ostringstream sout;
//     collatz_solve(sin, sout);
//     ASSERT_EQ(sout.str(), "1 1 1\n8 8 4\n5 5 6\n");}

// TEST(CollatzFixture, solve) {
//     istringstream sin("262144 262144\n262144 262144\n262144 262144\n262144 262144\n262144 262144\n262144 262144\n262144 262144\n");
//     ostringstream sout;
//     collatz_solve(sin, sout);
//     ASSERT_EQ(sout.str(), "1 1 1\n8 8 4\n5 5 6\n");}
// TEST(CollatzFixture, solve) {
//     istringstream sin("453 454\n");
//     ostringstream sout;
//     collatz_solve(sin, sout);
//     ASSERT_EQ(sout.str(), "1 1 1\n8 8 4\n5 5 6\n");}

// TEST(CollatzFixture, solve) {
//     istringstream sin("453 454\n");
//     ostringstream sout;
//     collatz_solve(sin, sout);
//     ASSERT_EQ(sout.str(), "1 1 1\n8 8 4\n5 5 6\n");}






// TEST(CollatzFixture, solve) {
//     istringstream sin("453119 454876\n");
//     ostringstream sout;
//     collatz_solve(sin, sout);
//     ASSERT_EQ(sout.str(), "1 1 1\n8 8 4\n5 5 6\n");}

//     TEST(CollatzFixture, solve) {
//     istringstream sin("45 54\n 50 54\n");
//     ostringstream sout;
//     collatz_solve(sin, sout);
//     ASSERT_EQ(sout.str(), "1 1 1\n8 8 4\n5 5 6\n");}

//     TEST(CollatzFixture, solve) {
//     istringstream sin("453 454\n");
//     ostringstream sout;
//     collatz_solve(sin, sout);
//     ASSERT_EQ(sout.str(), "1 1 1\n8 8 4\n5 5 6\n");}

//     TEST(CollatzFixture, solve) {
//     istringstream sin("453 454\n");
//     ostringstream sout;
//     collatz_solve(sin, sout);
//     ASSERT_EQ(sout.str(), "1 1 1\n8 8 4\n5 5 6\n");}
//     TEST(CollatzFixture, solve) {
//     istringstream sin("453 454\n");
//     ostringstream sout;
//     collatz_solve(sin, sout);
//     ASSERT_EQ(sout.str(), "1 1 1\n8 8 4\n5 5 6\n");}
//     TEST(CollatzFixture, solve) {
//     istringstream sin("453 454\n");
//     ostringstream sout;
//     collatz_solve(sin, sout);
//     ASSERT_EQ(sout.str(), "1 1 1\n8 8 4\n5 5 6\n");}
//     TEST(CollatzFixture, solve) {
//     istringstream sin("453 454\n");
//     ostringstream sout;
//     collatz_solve(sin, sout);
//     ASSERT_EQ(sout.str(), "1 1 1\n8 8 4\n5 5 6\n");}
//     TEST(CollatzFixture, solve) {
//     istringstream sin("453 454\n");
//     ostringstream sout;
//     collatz_solve(sin, sout);
//     ASSERT_EQ(sout.str(), "1 1 1\n8 8 4\n5 5 6\n");}
//     TEST(CollatzFixture, solve) {
//     istringstream sin("453 454\n");
//     ostringstream sout;
//     collatz_solve(sin, sout);
//     ASSERT_EQ(sout.str(), "1 1 1\n8 8 4\n5 5 6\n");}

