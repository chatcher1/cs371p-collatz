// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "Collatz.hpp"

using namespace std;
int * smallCache = new int[1000];

//calculates collatz sequence length for one number
int lengthSeq(long initial) {
    int length = 0;
    long val = initial;
    while(val != 1) {
        if(val%2 == 0) {
            val = val/2;
            length++;
        }
        else {
            val = (val*3 + 1)/2;
            length+=2;
        }
    }
    return length + 1;
}

//find the maximum sequence length for the range
int findMaxLen(int lower, int upper) {
    int maxLen = 0;
    while(lower <= upper) {
        int length = lengthSeq((long)lower);
        if(length > maxLen) {
            maxLen = length;
        }


        lower++;

    }
    return maxLen;

}
// ------------
// collatz_read
// ------------
void create_cache() {
    int index = 0;

    while(index < 1000) {
        int startRange = 1000*(index) + 1;
        int endRange = 1000*(index+1);
        int maxLen = 0;
        while(startRange <= endRange && startRange < 1000000) {
            int length = lengthSeq((long)startRange);
            startRange++;
            if(length > maxLen) {
                maxLen = length;
            }
        }

        smallCache[index] = maxLen;

        index++;
    }

}

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i, j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// collatz_eval
// ------------

tuple<int, int, int> collatz_eval (const pair<int, int>& p) {
    int i, j;
    tie(i, j) = p;

    //initial assertions to check that i and j follow the necessary limits
    assert(i > 0);
    assert(j > 0);


    assert(j < 1000000);
    assert(i < 1000000);

    //see whether i or j is lower and assigns them to appropriate variables, use the trick to reduce range
    int lower = i <= j? i : j;
    int upper = i <= j ? j : i;
    assert(lower <= upper);
    lower = lower < upper/2 + 1 ? upper/2 + 1: lower;
    assert(lower <= upper);
    //if upper and lower are less than 1000 apart take normal approach
    //otherwise use the smallCache to limit the number of sequence lengths to calculate
    int maxLen = 0;
    int sizeOfCache = 1000;
    int lowerInd = (lower/sizeOfCache+1)*sizeOfCache;

    int upperInd = (upper/sizeOfCache) * sizeOfCache;

    if(upper-lower < sizeOfCache) {
        maxLen = max(maxLen, findMaxLen(lower, upper));
    }
    else {
        //potentially in three chunks: lower to the lowest multiple of 1000 that's greater,
        //all the ranges that can be fully covered by the cache, and the highest multiple of 1000
        //that's less than upper to upper
        maxLen = max(maxLen, findMaxLen(lower, lowerInd));

        lowerInd++;
        while(lowerInd < upperInd) {
            maxLen = max(maxLen, smallCache[(lowerInd-1)/sizeOfCache]);
            lowerInd++;
        }
        upperInd++;
        maxLen = max(maxLen, findMaxLen(upperInd, upper));

    }

    assert(maxLen > 0);

    return make_tuple(i, j, maxLen);
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i, j, v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    create_cache();
    string s;
    while (getline(sin, s))
        collatz_print(sout, collatz_eval(collatz_read(s)));
}
