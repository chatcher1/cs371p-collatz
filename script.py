import random
def main():
    with open("tests.txt","w") as fileObj:
        for i in range(200):
            firstInt = random.randint(1, 999999)
            secondInt = random.randint(1, 999999)
            fileObj.write(str(firstInt) + " " + str(secondInt))
            fileObj.write("\n")
    fileObj.close()
if __name__ == "__main__": main()