# CS371p: Object-Oriented Programming Collatz Repo

* Name: Caroline Hatcher

* EID: cwh2349

* GitLab ID: chatcher1

* HackerRank ID: carolinewhatcher

* Git SHA: (most recent Git SHA, final change to your repo will change this, that's ok)
2ebe240d584da653324c991835d6c3e3150ae594

* GitLab Pipelines: https://gitlab.com/chatcher1/cs371p-collatz/-/pipelines (link to your GitLab CI Pipeline)
https://gitlab.com/chatcher1/cs371p-collatz/-/pipelines/253981840
or https://gitlab.com/chatcher1/cs371p-collatz/-/pipelines/

I am not sure which one is needed.


* Estimated completion time: 7

* Actual completion time: 14

* Comments: I did underestimate how long it would take. Script.py is what I used to generate my acceptance tests. 

