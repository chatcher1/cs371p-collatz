// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "Collatz.hpp"

using namespace std;

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i, j;
    sin >> i >> j;
    return make_pair(i, j);}

// ------------
// collatz_eval
// ------------

tuple<int, int, int> collatz_eval (const pair<int, int>& p) {
    int i, j;
    tie(i, j) = p;
    assert(i > 0);
    assert(j > 0);


    assert(j < 1000000);
    assert(i < 1000000);
    // <your code>
    
    //int maxLen = 0;
    int index = 0;
    int * smallCache = new int[1000];
    while(index < 1000){
        int startRange = 1000*(index) + 1;
        int endRange = 1000*(index+1);
        int maxLen = 0;
        while(startRange <= endRange && startRange < 1000000){
            int length = 0;
            long copyPos = startRange;
            while(copyPos != 0){
            
  

                if(copyPos == 1){
                    length++;
                    break;
                }
                else if(copyPos%2 == 0){
                    copyPos = copyPos/2;
                    length++;
                }
                else{
                    copyPos = (copyPos*3 + 1)/2;
                    length+=2;
                }
            
        }
        startRange++;
        maxLen = max(maxLen, length);
    }
    //     //cout << "length for " << lower << "is " << length <<endl;
        smallCache[index] = maxLen;

    index++;
    }

    // cout << "HERE" << endl;

    
     int lower = i <= j? i : j;
    // //lower = lower < j/2 +1 ? j/2 + 1: lower;
    int upper = i <= j ? j : i;
    assert(lower <= upper);
    lower = lower < upper/2 + 1 ? upper/2 + 1: lower;
    // cout << "Hi " << lower << endl;

    // assert(lower <= upper);
    int maxLen = 0;
    int lowerInd = (lower/1000+1)*1000;

    int upperInd = (upper/1000) * 1000;
    int lowBound = lower;
    int highBound = upper;

    while(lower < lowerInd){
        long copyPos = lower;
         int length = 0;
         while(copyPos != 0){

            if(copyPos == 1){
                length++;
                break;
            }
           else if(copyPos%2 == 0){
               copyPos = copyPos/2;
                length++;
            }
            else{
                 copyPos = (copyPos*3 + 1)/2;
                length+=2;
            }
            
        }
        if(length > maxLen){
             maxLen = length;
        }
        

         lower++;

    }
    lowerInd++;
    while(lowerInd < upperInd){
        maxLen = max(maxLen, smallCache[(lowerInd-1)/1000]);
        lowerInd++;
    }
    upperInd++;
    while(upperInd < upper){
        long copyPos = upperInd;
         int length = 0;
         while(copyPos != 0){

            if(copyPos == 1){
                length++;
                break;
            }
           else if(copyPos%2 == 0){
               copyPos = copyPos/2;
                length++;
            }
            else{
                 copyPos = (copyPos*3 + 1)/2;
                length+=2;
            }
            
        }
        if(length > maxLen){
             maxLen = length;
        }
        

         upperInd++;

    }


    // if(lower > (upper/2 + 1)){
    //     lowerInd++;
    //     if(low)

    // }
    // while(lowerInd < upperInd){
    //     maxLen = max(maxLen, smallCache[lowerInd]);
    //     lowerInd++;
    // }
    // //int lowBound = lower < upperInd * 1000? upperInd*1000 : lower;
    // //int lowBound = lowerInd * 1000 == upperInd * 1000? lower : lowerInd*1000;
    // int highBound = upper;
    // cout << lowBound << " " << highBound << endl;
    // while(lowBound <= highBound){
    //     long copyPos = lowBound;
    //     int length = 0;
    //     while(copyPos != 0){
    //         if(copyPos == 1){
    //             length++;
    //             break;
    //         }
    //         else if(copyPos%2 == 0){
    //             copyPos = copyPos/2;
    //              length++;
    //          }
    //          else{
    //              copyPos = (copyPos*3 + 1)/2;
    //              length+=2;
    //          }
    // }
    // if(length > maxLen){
    //          maxLen = length;
    // }
    // lowBound++;
    // }


    //cout << lower << endl;
    // while(lower <= upper){
    // long copyPos = lower;
    //      int length = 0;
    //      while(copyPos != 0){
            
    //     if(copyPos < 1000000 && cache[copyPos-1]!= 0){
    // //         //     //cout << length << " new val" << copyPos << " " << (cache[copyPos-1]) << endl;
    //         length+=(cache[copyPos-1]);
    //         break;
    //     }
    // //         // }

    //         if(copyPos == 1){
    //             length++;
    //             break;
    //         }
    //        else if(copyPos%2 == 0){
    //            copyPos = copyPos/2;
    //             length++;
    //         }
    //         else{
    //              copyPos = (copyPos*3 + 1)/2;
    //             length+=2;
    //         }
            
    //     }
    //     if(length > maxLen){
    //          maxLen = length;
    //     }
        
    //      cache[lower-1] = length;

    //      lower++;
    // }
    
    assert(maxLen > 0);
    //delete[] cache;
    delete[] smallCache;
    //deleted cache
    return make_tuple(i, j, maxLen);}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i, j, v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    string s;
    while (getline(sin, s))
         collatz_print(sout, collatz_eval(collatz_read(s)));}
